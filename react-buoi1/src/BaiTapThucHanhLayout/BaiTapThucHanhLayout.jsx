import React, { Component } from "react";
import Banner from "../Banner/Banner";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import Item from "../Item/Item";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <div className="container d-flex p-0">
          <Item />
          <Item />
          <Item />
          <Item />
        </div>
        <Footer />
      </div>
    );
  }
}
